#include "OsModule.hpp"

OsModule 	OsModule::_singleton;

OsModule::OsModule() :
	AMonitorModule("Os")
{}

OsModule::OsModule(const OsModule& o) :
	AMonitorModule(o)
{}

OsModule&	OsModule::operator=(const OsModule& o)
{
	(void)o;
	return (*this);
}

OsModule::~OsModule()
{

}

int		OsModule::getRefreshInterval() const {
	return (1);
}

bool	OsModule::refresh() {
	return (false);
}