#include "HostUserModule.hpp"
#include <sys/sysctl.h>

HostUserModule	HostUserModule::_singleton;
HostUserModule::HostUserModule() : AMonitorModule("HostUser") {}
HostUserModule::HostUserModule(const HostUserModule& hu) : AMonitorModule(hu) {}
HostUserModule::~HostUserModule() {}

HostUserModule& HostUserModule::operator=(const HostUserModule& hu) {  (void)hu; return (*this);  }

int		HostUserModule::getRefreshInterval() const {
	return (5);
}

bool	HostUserModule::refresh() {
	int			mib[2];
	size_t		len;
	char		*hostname;
	mib[0] = CTL_KERN;
	mib[1] = KERN_HOSTNAME;
	sysctl(mib, 2, NULL, &len, NULL, 0);
	hostname = new char(len);
	sysctl(mib, 2, hostname, &len,  NULL, 0);
	if (!this->_datas.find("hostname")->second.compare(hostname))
		return (false);
	else {
		this->_datas["hostname"] = hostname;
		return (true);
	}
}
