#include "ModuleManager.hpp"

ModuleManager	ModuleManager::_singleton;

ModuleManager::ModuleManager()
{

}

ModuleManager::~ModuleManager()
{
}

ModuleManager::ModuleManager(const ModuleManager& mm)
{
	(void)mm;
}

ModuleManager&	ModuleManager::operator=(const ModuleManager& mm)
{
	(void)mm;
	return (*this);
}

ModuleManager*	ModuleManager::registerModule(AMonitorModule *mod)
{
	this->_modules[mod->getName()] = mod;
	return (this);
}

AMonitorModule*	ModuleManager::getModule(const std::string& name)
{
	std::map<std::string, AMonitorModule*>::iterator it = this->_modules.find(name);
	if (it != this->_modules.end())
		return (*it).second;
	return (0);
}
