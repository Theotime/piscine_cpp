#include "AMonitorModule.hpp"
#include "ModuleManager.hpp"

AMonitorModule::AMonitorModule()
{

}

AMonitorModule::~AMonitorModule()
{

}

AMonitorModule::AMonitorModule(const std::string& name) :
	_activate(false),
	_name(name)
{

}

AMonitorModule::AMonitorModule(const AMonitorModule& m) :
	_activate(m._activate),
	_name(m._name)
{

}

AMonitorModule&	AMonitorModule::operator=(const AMonitorModule& m)
{
	this->_activate = m._activate;
	this->_name = m._name;
	return (*this);
}

std::map< std::string, std::string >	AMonitorModule::getDatas() const
{
	return (this->_datas);
}
