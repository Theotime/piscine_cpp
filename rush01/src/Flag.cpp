#include "Flag.hpp"
#include <iostream>

Flag	Flag::_singleton;

Flag::Flag()
{

}

Flag::~Flag()
{

}

Flag::Flag(const Flag& f)
{
	(void)f;
}

Flag&	Flag::operator=(const Flag& f)
{
	(void)f;
	return (*this);
}

Flag*	Flag::addShort(const char flag)
{
	this->_flags_short[flag] = false;
	return (this);
}

Flag*	Flag::addLong(const std::string& flag, char flag_short)
{
	this->_flags_long[flag] = false;
	if (flag_short)
		return (this->addShort(flag_short));
	return (this);
}

Flag*	Flag::addDataFlag(const std::string& flag, void (*f)(const std::string&))
{
	this->_flags_handler[flag] = f;
	return (this);
}

bool	Flag::parse(int ac, char **av)
{
	for (int i = 1; i < ac; ++i) {
		if (av[i][0] == '-' && av[i][1] == '-' && !av[i][2])
			return (this);
		else if (av[i][0] == '-' && av[i][1] == '-') {
			std::string flag = std::string(av[i] + 2);
			std::string	data;
			size_t index;
			if ((index = flag.find_first_of('=')) != std::string::npos) {
				data = flag.substr(index + 1);
				flag = flag.substr(0, index);
			}
			if (data.empty())
			{
				std::map<std::string, bool>::iterator it = this->_flags_long.find(flag);
				if (it != this->_flags_long.end()) {
					(*it).second = true;
				} else {
					std::cerr << av[0] << ": Flag --" << flag << " unsuported" << std::endl;
					return (false);
				}
			} else {
				std::map<std::string, void (*)(const std::string&)>::iterator it = this->_flags_handler.find(flag);
				if (it != this->_flags_handler.end()) {
					(*it).second(data);
				} else {
					std::cerr << av[0] << ": Flag --" << flag << " unsuported" << std::endl;
					return (false);
				}
			}
		} else if (av[i][0] == '-') {
			int j = 1;
			while (av[i][j]) {
				std::map<char, bool>::iterator it = this->_flags_short.find(av[i][j]);
				if (it != this->_flags_short.end()) {
					(*it).second = true;
				} else {
					std::cerr << av[0] << ": Flag -" << av[i][j] << " unsuported" << std::endl;
					return (false);
				}
				++j;
			}
		}
	}
	return (true);
}

bool	Flag::hasShort(const char flag) const
{
	std::map<char, bool>::const_iterator it = this->_flags_short.find(flag);
	if (it != this->_flags_short.end())
		return (*it).second;
	return (false);
}

bool	Flag::hasLong(const std::string& flag) const
{
	std::map<std::string, bool>::const_iterator it = this->_flags_long.find(flag);
	if (it != this->_flags_long.end())
		return (*it).second;
	return (false);
}