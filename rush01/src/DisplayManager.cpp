#include "DisplayManager.hpp"

DisplayManager 	DisplayManager::_singleton;

DisplayManager::DisplayManager() :
	_console(0),
	_gui(0),
	_is_console(true)
{

}

DisplayManager::~DisplayManager()
{

}

DisplayManager::DisplayManager(const DisplayManager& dm)
{
	(void)dm;
}

DisplayManager&	DisplayManager::operator=(const DisplayManager& dm)
{
	(void)dm;
	return (*this);
}