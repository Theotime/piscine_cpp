/**
 * Main de test dedié a l'utilisation du display ncurses pour le projet:
 * FT_GKRELLM
 **/

#include "primary.hpp"
#include "Display.class.hpp"
#include "Window/Module.class.hpp"
#include "Modules/CPU.class.hpp"
#include "Modules/Ram.class.hpp"

int			main () {
	Display			display;
	Modules::Ram	*ramModule;
	Modules::CPU	*cpuModule;

	display.init();

	ramModule = new Modules::Ram(display.getWindow(), 70, 10, 1, 1);
	cpuModule = new Modules::CPU(display.getWindow(), 70, 10, 70 + 2, 1);

	cpuModule->print("Usage : 10\%")->apply();
	ramModule->print("Usage : 10Go/16Go")->apply();

	display.end();
	return (0);
}
