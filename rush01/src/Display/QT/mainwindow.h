#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QMovie>
#include <QLabel>
#include <QGroupBox>
#include <QtWidgets>
#include <vector>
#include <string>
#include "../../../include/IMonitorDisplay.hpp"
#include "../../../include/ModuleManager.hpp"
#include "../../../include/HostUserModule.hpp"
#include "../../../include/DateTimeModule.hpp"
#include "../../../include/OsModule.hpp"
#include "../../../include/CpuModule.hpp"
#include "../../../include/RamModule.hpp"
#include "../../../include/NetworkModule.hpp"

class MainWindow : public QMainWindow, public IMonitorDisplay
{
    Q_OBJECT
public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void     init(void);
    void     update(void);

signals:

public slots:

private:

    MainWindow(MainWindow const & src);
    MainWindow &            operator=(MainWindow const & rhs);

    void                        _initModuleManager(void);

    void                        _setWidgets(void);
    void                        _setBoxes(void);
    void                        _setGroupboxes(void);
    void                        _setProgressBar(void);
    void                        _makeZazHappy(void);
    void                        _createLayout(void);

    void                        _updateGeneralInfo(void);
    void                        _updateCpuInfo(void);

    QWidget *                   _page1;
    QWidget *                   _page2;
    QWidget *                   _page3;
    QWidget *                   _page4;

    QTabWidget *                _onglets;

    QVBoxLayout *               _vbox1;
    QVBoxLayout *               _vbox2;
    QVBoxLayout *               _hbox1;
    QVBoxLayout *               _hbox2;

    QGroupBox *                 _groupbox;
    QGroupBox *                 _groupbox2;
    QGroupBox *                 _groupbox3;

    QLabel *                    _os;
    QLabel *                    _host;
    QLabel *                    _user;
    QLabel *                    _time;
    QLabel *                    _zaz;

    QProgressBar *              _cpu;
    QProgressBar *              _cpu2;
    QProgressBar *              _cpu3;
    QProgressBar *              _cpu4;

//    std::vector<QProgressBar>   _cpu;
    QMovie *                    _movie;
};

#endif // MAINWINDOW_H
