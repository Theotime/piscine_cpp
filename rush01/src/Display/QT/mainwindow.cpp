#include "mainwindow.h"

/*
 *  Public
 * */

//  CONSTRUCTORS

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    init();
    return ;
}

void     MainWindow::_setWidgets(void) {
    this->_onglets = new QTabWidget(this);
    this->_onglets->setGeometry(10, 10, 780, 580);

    // onglet titres
    this->_page1 = new QWidget;
    this->_page2 = new QWidget;
    this->_page3 = new QWidget;
    this->_page4 = new QWidget;
    this->_zaz = new QLabel;
    this->_onglets->addTab(this->_page1, "System Info");
    this->_onglets->addTab(this->_page2, "Hardware Info");
    this->_onglets->addTab(this->_page3, "Open Files");
    this->_onglets->addTab(this->_page4, "Open Soft");
    this->_onglets->addTab(this->_zaz, "About");
}

void     MainWindow::_setBoxes(void) {
    //gird vertical
    this->_vbox1 = new QVBoxLayout;
    this->_vbox2 = new QVBoxLayout;
    //gird horizon pour box1
    this->_hbox1 = new QVBoxLayout; // gird horizon
    //gird horizon pour box1
    this->_hbox2 = new QVBoxLayout;
}

void     MainWindow::_setGroupboxes(void) {
    //elem gird vert
    this->_groupbox = new QGroupBox("Summary ");
    this->_groupbox->move(5, 5);
    //elem gird horizon grp1
    this->_os = new QLabel(this->_groupbox);
    this->_host = new QLabel(this->_groupbox);
    this->_user = new QLabel(this->_groupbox);
    this->_time = new QLabel(this->_groupbox);
    this->_groupbox2 = new QGroupBox("CPU Info");
    this->_groupbox->move(5, 5);
    this->_groupbox3 = new QGroupBox("RAM Info");
    this->_groupbox->move(5, 5);
}

void     MainWindow::_setProgressBar(void) {
    //elem gird horizon grp1
    this->_cpu = new QProgressBar(this->_groupbox2);
    this->_cpu2 = new QProgressBar(this->_groupbox2);
    this->_cpu3 = new QProgressBar(this->_groupbox2);
    this->_cpu4 = new QProgressBar(this->_groupbox2);
}

void     MainWindow::_updateGeneralInfo(void) {
    this->_os->setText("Operating System: Darwin");
    this->_host->setText("hostname: e3r5p12");
    this->_user->setText("username: CCHEVALL");
    this->_time->setText("DATE / TIME: 12/12/12 12:12");
    this->_os->update();
    this->_host->update();
    this->_user->update();
    this->_time->update();
}

void     MainWindow::_updateCpuInfo(void) {
    this->_cpu->setValue(10);
    this->_cpu2->setValue(20);
    this->_cpu3->setValue(30);
    this->_cpu4->setValue(40);
    this->_cpu->update();
    this->_cpu2->update();
    this->_cpu3->update();
    this->_cpu4->update();
}

void     MainWindow::_makeZazHappy(void) {
    this->_movie = new QMovie(":/ressources/ressources/zaz.gif");
    this->_zaz->setMovie(this->_movie);
    this->_movie->start();
}

void     MainWindow::_createLayout(void) {
    //add elem to gird
    this->_vbox1->addLayout(this->_hbox1);
    this->_vbox1->addWidget(this->_groupbox);
    this->_vbox1->addWidget(this->_groupbox2);
    this->_vbox1->addWidget(this->_groupbox3);

    //add elem to hgird
    //result page 1
    this->_page1->setLayout(this->_vbox1);
    this->_page2->setLayout(this->_vbox2);
}

void     MainWindow::_initModuleManager(void) {
//    ModuleManager::instance()->registerModule(HostUserModule::instance());
//    ModuleManager::instance()->registerModule(DateTimeModule::instance());
//    ModuleManager::instance()->registerModule(OsModule::instance());
//    ModuleManager::instance()->registerModule(CpuModule::instance());
//    ModuleManager::instance()->registerModule(RamModule::instance());
//    ModuleManager::instance()->registerModule(NetworkModule::instance());
}

void     MainWindow::init(void) {
    setWindowFlags(Qt::FramelessWindowHint);
    setWindowFlags(Qt::Tool);
    setFixedSize(800, 600);
    setWindowTitle("42 monitor | v0.0");

    this->_initModuleManager();
    this->_setWidgets();
    this->_setBoxes();
    this->_setGroupboxes();
    this->_setProgressBar();
    this->_makeZazHappy();
    this->_createLayout();
    //add elem to gird
    this->_hbox1->addWidget(this->_os);
    this->_hbox1->addWidget(this->_time);
    this->_hbox1->addWidget(this->_host);
    this->_hbox1->addWidget(this->_user);
    this->_hbox1->setAlignment(Qt::AlignCenter);
    this->_groupbox->setLayout(this->_hbox1);

    //add elem to girdx
    this->_hbox2->addWidget(this->_cpu);
    this->_hbox2->addWidget(this->_cpu2);
    this->_hbox2->addWidget(this->_cpu3);
    this->_hbox2->addWidget(this->_cpu4);
    this->_hbox2->setAlignment(Qt::AlignTop);
    this->_groupbox2->setLayout(this->_hbox2);

    this->update();
}

void     MainWindow::update(void) {
    this->_updateGeneralInfo();
    this->_updateCpuInfo();
}

/*
 * Private
 * */

//  CONSTRUCTORS

MainWindow::MainWindow( MainWindow const & src) : QMainWindow() {
    static_cast<void>(src);
    return ;
}

MainWindow &            MainWindow::operator=(MainWindow const & rhs) {
    static_cast<void>(rhs);
    return (*this);
}

MainWindow::~MainWindow()
{
    return ;
}
