#include "mainwindow.h"
#include <QThread>

int main(int ac, char**av)
{
    QApplication app(ac, av);
    QApplication::setWindowIcon(QIcon(":/ressources/myapp.icns"));
    MainWindow w;
    w.update();
    w.show();

    for (int i = 0; i < 100; i++) {
        w.update();
        app.processEvents();
//        QThread::msleep(0);
    }
    return app.exec();
}
