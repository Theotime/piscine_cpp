QT       += widgets

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

RESOURCES += \
    ressources.qrc

ICON = ressources/myapp.icns
