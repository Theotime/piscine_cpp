#include "DateTimeModule.hpp"
#include <ctime>

DateTimeModule DateTimeModule::_singleton;

DateTimeModule::DateTimeModule()
	: AMonitorModule("DateTime")
{
}

DateTimeModule::~DateTimeModule()
{
}

DateTimeModule::DateTimeModule(const DateTimeModule& obj)
	: AMonitorModule(obj)
{
}

DateTimeModule& DateTimeModule::operator=(const DateTimeModule& obj)
{
	(void)obj;
	return (*this);
}

int		DateTimeModule::getRefreshInterval() const {
	return (1);
}

bool	DateTimeModule::refresh()
{
	time_t t;
	std::time(&t);
	std::string str(std::ctime(&t));
	str = str.substr(0, str.length() - 1);
	if (AMonitorModule::_datas["current"].compare(str))
	{
		AMonitorModule::_datas["current"] = str;
		return (true);
	}
	return (false);
}
