#include <iostream>
#include <string>
#include <unistd.h>
#include "Flag.hpp"
#include "ModuleManager.hpp"
#include "DisplayManager.hpp"
#include "HostUserModule.hpp"
#include "DateTimeModule.hpp"

void	activateModule(const std::string& module)
{
	AMonitorModule	*mod = ModuleManager::instance()->getModule(module);
	if (!mod) {
		std::cerr << "Module [" << module << "] does not exist" << std::endl;
	} else {
		mod->setActive(true);
		std::cout << "Module: " << mod->getName() << " is active" << std::endl;
	}
}

int	main(int ac, char **av)
{
	ModuleManager::instance()->registerModule(HostUserModule::instance());
	ModuleManager::instance()->registerModule(DateTimeModule::instance());
	Flag::instance()->addLong("gui", 'g');
	Flag::instance()->addDataFlag("module", activateModule);
	if (Flag::instance()->parse(ac, av)) {
		if (Flag::instance()->hasLong("gui") || Flag::instance()->hasShort('g')) {
			DisplayManager::instance()->selectConsole(false);
		}
		IMonitorDisplay *dis = DisplayManager::instance()->getCurrentDisplay();
		if (dis) {
			dis->init();
			dis->update();
		} else {
			while (1) {
				if (DateTimeModule::instance()->refresh()) {
					std::cout << DateTimeModule::instance()->getDatas()["current"] << std::endl;
					system("zsh -c 'tot=0 ; for i in `ps -eo pcpu | sed \"1d\"`; do tot=$((tot + i)) ; done ; tot=$((tot / 4)) ; echo $tot'");
				}
				usleep(1000);
			}
			// std::cerr << "Display cannot be found" << std::endl;
			// return (2);
		}
	} else
		return (1);
}
