#include "NetworkModule.hpp"

NetworkModule NetworkModule::_singleton;

NetworkModule::NetworkModule() :
	AMonitorModule("Network")
{
}

NetworkModule::~NetworkModule()
{
}

NetworkModule::NetworkModule(const NetworkModule& obj) :
	AMonitorModule(obj)
{
	(void)obj;
}

NetworkModule& NetworkModule::operator=(const NetworkModule& obj)
{
	(void)obj;
	return (*this);
}

int		NetworkModule::getRefreshInterval() const {
	return (1);
}

bool	NetworkModule::refresh() {
	return (false);
}