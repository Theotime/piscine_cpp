#include "RamModule.hpp"

RamModule RamModule::_singleton;

RamModule::RamModule()
	: AMonitorModule("RAM")
{
}

RamModule::~RamModule()
{
}

RamModule::RamModule(const RamModule& obj)
	: AMonitorModule(obj)
{
}

RamModule& RamModule::operator=(const RamModule& obj)
{
	(void)obj;
	return (*this);
}

int		RamModule::getRefreshInterval() const {
	return (1);
}

bool	RamModule::refresh() {
	return (false);
}