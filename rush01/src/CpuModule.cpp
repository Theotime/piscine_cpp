#include "CpuModule.hpp"

CpuModule CpuModule::_singleton;

CpuModule::CpuModule() :
	AMonitorModule("CPU")
{
}

CpuModule::~CpuModule()
{
}

CpuModule::CpuModule(const CpuModule& obj) :
	AMonitorModule(obj)
{
	(void)obj;
}

CpuModule& CpuModule::operator=(const CpuModule& obj)
{
	(void)obj;
	return (*this);
}

int		CpuModule::getRefreshInterval() const {
	return (1);
}

bool	CpuModule::refresh() {
	// system("tot=0 ; for i in `ps -eo pcpu | sed '1d'`; do tot=$((tot + i)) ; done ; echo $tot");
	return (false);
}