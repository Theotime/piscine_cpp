#ifndef AMONITORMODULE_HPP
# define AMONITORMODULE_HPP

# include <string>
# include "IMonitorModule.hpp"

class AMonitorModule : public IMonitorModule
{
private:
	bool													_activate;
	std::string												_name;
	
	AMonitorModule();
protected:
	std::map<std::string, std::string>						_datas;

public:
	AMonitorModule(const std::string& name);
	virtual ~AMonitorModule();
	AMonitorModule(const AMonitorModule& m);
	AMonitorModule& operator=(const AMonitorModule& m);

	inline AMonitorModule*	setActive(bool activate) {
		this->_activate = activate;
		return (this);
	}
	inline bool				isActive() const {
		return (this->_activate);
	}
	inline const std::string	&getName() const {
		return (this->_name);
	}

	virtual std::map<std::string, std::string>	getDatas() const;
};

#endif