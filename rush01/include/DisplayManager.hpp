#ifndef DISPLAYMANAGER_HPP
# define DISPLAYMANAGER_HPP

# include "IMonitorDisplay.hpp"

class DisplayManager
{
private:
	IMonitorDisplay	*_console;
	IMonitorDisplay	*_gui;
	bool			_is_console;

	DisplayManager();
	DisplayManager(const DisplayManager& dm);
	DisplayManager& operator=(const DisplayManager& dm);

	static DisplayManager 	_singleton;
public:
	~DisplayManager();

	static DisplayManager*		instance() {
		return (&DisplayManager::_singleton);
	}

	inline DisplayManager*		selectConsole(bool console) {
		this->_is_console = console;
		return (this);
	}

	inline IMonitorDisplay*		getCurrentDisplay() {
		return (this->_is_console ? this->_console : this->_gui);
	}
};

#endif