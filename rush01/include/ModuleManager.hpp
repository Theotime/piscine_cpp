#ifndef MODULEMANAGER_HPP
# define MODULEMANAGER_HPP

# include "AMonitorModule.hpp"
# include <map>

class ModuleManager
{
private:
	ModuleManager();
	ModuleManager(const ModuleManager& mm);
	ModuleManager& operator=(const ModuleManager& mm);

	static ModuleManager 					_singleton;
	std::map<std::string, AMonitorModule*>	_modules;

public:
	~ModuleManager();

	ModuleManager*			registerModule(AMonitorModule *mod);

	AMonitorModule*			getModule(const std::string& name);

	static ModuleManager*	instance() {
		return (&ModuleManager::_singleton);
	}
};

#endif