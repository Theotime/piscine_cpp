#ifndef _RAMMODULE_HPP
# define _RAMMODULE_HPP

# include "AMonitorModule.hpp"

class RamModule : public AMonitorModule
{
private:
	static RamModule _singleton;
	RamModule(const RamModule& obj);
	RamModule& operator=(const RamModule& obj);
	RamModule();

public:	
	virtual ~RamModule();

	virtual int							getRefreshInterval() const;
	virtual bool						refresh();

	static inline RamModule*	instance() {
		return (&RamModule::_singleton);
	}
};
#endif
