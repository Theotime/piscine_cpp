#ifndef _NETWORKMODULE_HPP
# define _NETWORKMODULE_HPP

# include "AMonitorModule.hpp"

class NetworkModule : public AMonitorModule
{
private:
	static NetworkModule _singleton;

	NetworkModule();
	NetworkModule(const NetworkModule& obj);
	NetworkModule& operator=(const NetworkModule& obj);
public:
	virtual ~NetworkModule();

	virtual int							getRefreshInterval() const;
	virtual bool						refresh();

	static inline NetworkModule*	instance() {
		return (&NetworkModule::_singleton);
	}
};

#endif
