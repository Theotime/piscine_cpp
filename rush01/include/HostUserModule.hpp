#ifndef HOSTUSERMODULE_HPP
# define HOSTUSERMODULE_HPP

# include "AMonitorModule.hpp"
# include <string>

class HostUserModule : public AMonitorModule
{
private:
	HostUserModule();
	HostUserModule(const HostUserModule& hu);
	HostUserModule& operator=(const HostUserModule& hu);

	static HostUserModule 	_singleton;

public:
	virtual ~HostUserModule();
	virtual int 												getRefreshInterval() const;
	virtual bool												refresh();

	static inline HostUserModule*	instance() {
		return (&HostUserModule::_singleton);
	}
};

#endif
