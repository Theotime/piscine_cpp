#ifndef FLAG_HPP
# define FLAG_HPP

# include <string>
# include <map>

class Flag
{
private:
	Flag();
	Flag(const Flag& f);
	Flag& operator=(const Flag& f);

	static Flag 	_singleton;

	std::map<std::string, bool>							_flags_long;
	std::map<char, bool>								_flags_short;
	std::map<std::string, void (*)(const std::string&)>	_flags_handler;

public:
	~Flag();

	Flag*	addShort(const char flag);
	Flag*	addLong(const std::string& flag, char flag_short = 0);
	Flag*	addDataFlag(const std::string& flag, void (*)(const std::string&));

	bool	parse(int ac, char **av);

	bool	hasShort(const char flag) const;
	bool	hasLong(const std::string& flag) const;

	static inline Flag*	instance() {
		return (&Flag::_singleton);
	}
};

#endif