#ifndef IMONITORDISPLAY_HPP
# define IMONITORDISPLAY_HPP

class IMonitorDisplay
{
public:
	virtual ~IMonitorDisplay() {}
	virtual void	init() = 0;
	virtual void	update() = 0;	
};

#endif