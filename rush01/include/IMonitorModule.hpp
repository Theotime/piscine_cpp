#ifndef IMONITORMODULE_HPP
# define IMONITORMODULE_HPP

# include <map>
# include <string>

class IMonitorModule
{
public:
	virtual ~IMonitorModule() {}
	virtual int 										getRefreshInterval() const = 0;
	virtual bool										refresh() = 0;
	virtual std::map<std::string, std::string> 			getDatas() const = 0;
};

#endif