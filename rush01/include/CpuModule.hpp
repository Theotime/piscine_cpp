#ifndef _CPUMODULE_HPP
# define _CPUMODULE_HPP

# include "AMonitorModule.hpp"

class CpuModule : public AMonitorModule
{

private:
	static CpuModule 	_singleton;

	CpuModule(const CpuModule& obj);
	CpuModule& operator=(const CpuModule& obj);
	CpuModule();
public:	
	virtual ~CpuModule();
	
	virtual int							getRefreshInterval() const;
	virtual bool						refresh();

	static inline CpuModule*	instance() {
		return (&CpuModule::_singleton);
	}
};
#endif
