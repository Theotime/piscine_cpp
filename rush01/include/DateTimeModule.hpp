#ifndef _DATETIMEMODULE_HPP
# define _DATETIMEMODULE_HPP

# include "AMonitorModule.hpp"

class DateTimeModule : public AMonitorModule
{

private:
	DateTimeModule();
	DateTimeModule(const DateTimeModule& obj);
	DateTimeModule& operator=(const DateTimeModule& obj);

	static DateTimeModule _singleton;
protected:

public:
	virtual ~DateTimeModule();

	virtual int		getRefreshInterval() const;
	virtual bool	refresh();

	static inline DateTimeModule*	instance() {
		return (&DateTimeModule::_singleton);
	}

};
#endif
