#ifndef OSMODULE_HPP
# define OSMODULE_HPP

# include "AMonitorModule.hpp"

class OsModule : public AMonitorModule
{
private:
	OsModule();
	OsModule(const OsModule& o);
	OsModule& operator=(const OsModule& o);

	static OsModule 	_singleton;
public:
	virtual ~OsModule();

	virtual int							getRefreshInterval() const;
	virtual bool						refresh();

	static inline OsModule*	instance() {
		return (&OsModule::_singleton);
	}
};

#endif