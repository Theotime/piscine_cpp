
#ifndef ENEMY_HPP
# define ENEMY_HPP

class Enemy
{
	private:
	public:
		Enemy();
		Enemy(const Enemy& obj);
		~Enemy();
		Enemy& operator=(const Enemy& obj);
};
#endif
