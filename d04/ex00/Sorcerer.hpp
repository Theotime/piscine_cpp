/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/10 00:02:38 by triviere          #+#    #+#             */
/*   Updated: 2015/01/10 04:20:01 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __SORCERER_HPP__
# define __SORCERER_HPP__

#include "Victim.hpp"
#include <string>
#include <iostream>

class Sorcerer {
	public:
		Sorcerer();
		Sorcerer(std::string name, std::string title);
		Sorcerer(Sorcerer const &obj);
		~Sorcerer();


		std::string		getName()							const;
		std::string		getTitle()							const;

		void			polymorph(Victim const &victim)		const;

		Sorcerer		&operator=(Sorcerer const &o);

	private:
		std::string		_name;
		std::string		_title;

		void			born();
		void			die();
};

std::ostream			&operator<<(std::ostream &str, Sorcerer const &o);

#endif
