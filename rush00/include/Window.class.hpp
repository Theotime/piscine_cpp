#ifndef WINDOW_CLASS_HPP
#define WINDOW_CLASS_HPP

#include <iostream>
#include "ncurses.h"

#define XTOTAL 100
#define YTOTAL 20
#define YAFF 3
#define YP 20

class Window {

protected:
	int			_x;
	int			_y;
	int			_height;
	int			_width;
	WINDOW		*_win;

public:
	Window(void);
	Window(Window const& src);
	virtual ~Window(void);

	virtual void		newWin(int, int, int, int,bool d);
	void		put(std::string const&, std::string const&);

	inline int			getWidth() const {
		return(this->_width);
	};
	inline int			getHeigth()    const {
		return(this->_height);
	};
	inline WINDOW*		getWin() {
		return (this->_win);
	}
	inline int			getX() const {
		return (this->_x);
	}
	inline int			getY() const {
		return (this->_y);
	}

	Window& operator=(Window const& rhs);

};

std::ostream& operator<<(std::ostream& o, Window const& i);

#endif //!WINDOW_CLASS_HPP
