#ifndef DISPLAY_CLASS_HPP
#define DISPLAY_CLASS_HPP

#include <string>
#include <iostream>
#include "ncurses.h"
#include "Window.class.hpp"
#include "MoveWindow.class.hpp"
#include "Object.class.hpp"
#include "Utils/Map.class.hpp"
#include <unistd.h>
#include <termios.h>




class Display {

private:
		Window									_up;
		Window									_down;
		Window									_map;
		Utils::Map<Object*, MoveWindow*>		_obj;

		void			_initNcurses();
		void			_init();
		void			_nonBlock(int) const;

public:
	Display(void);
	Display(Display const& src);
	~Display(void);

	void			display(Object*);
	void			refresh();
	void			undisplay(Object*);

	inline int		getWidth() const {
		return (this->_map.getWidth());
	}
	inline int		getHeigth() const {
		return (this->_map.getHeigth());
	}
	inline void		putUp(std::string const& s, std::string const& s2) {
		this->_up.put(s, s2);
	}
	inline void		putDown(std::string const& s, std::string const& s2) {
		this->_down.put(s, s2);
		::wrefresh(this->_down.getWin());
	}

	Display& operator=(Display const& rhs);

};

std::ostream& operator<<(std::ostream& o, Display const& i);

#endif //!DISPLAY_CLASS_HPP
