#ifndef ROTOR_CLASS_HPP
# define ROTOR_CLASS_HPP

# include <iostream>
# include <unistd.h>
# include <ctime>

class Rotor {

	public:
		Rotor(void);
		Rotor(Rotor const& src);
		Rotor(int frame);
		virtual ~Rotor(void);

		unsigned int		getNbFramePerSec(void)			const;
		unsigned int		getNbFrame()					const;
		unsigned int		getTimePerFrame()				const;
		clock_t				getTime(unsigned int number)	const;
		bool				getEnd()						const;

		Rotor				*setNbFramePerSec(unsigned int nb_frame_per_sec);
		Rotor				*setEnd(bool etas);

		void				run();
		virtual void		onFrame();

		Rotor&				operator=(Rotor const &rhs);

	private:
		unsigned int		_nb_frame;
		unsigned int		_nb_frame_per_sec;
		bool				_end;
		clock_t				_time_per_frame;
		clock_t				_last_time_clock;

};

std::ostream& operator<<(std::ostream &o, Rotor const &i);

#endif //!ROTOR_CLASS_HPP
