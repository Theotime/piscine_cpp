#ifndef SPACEOBJECT_CLASS_HPP
#define SPACEOBJECT_CLASS_HPP

# include <iostream>
# include "Object.class.hpp"

class SpaceObject : public Object {

public:
	SpaceObject(void);
	SpaceObject(int power, int speed);
	SpaceObject(SpaceObject const& src);
	~SpaceObject(void);

	int		getPower(void) const;
	int		getSpeed(void) const;

	inline SpaceObject 	*setPower(int power) {
		this->_power = power;
		return (this);
	}

	inline SpaceObject 	*setSpeed(int speed) {
		this->_speed = speed;
		return (this);
	}

	SpaceObject& operator=(SpaceObject const& rhs);

private:
	int		_power;
	int		_speed;

};

std::ostream& operator<<(std::ostream& o, SpaceObject const& i);

#endif //!SPACEOBJECT_CLASS_HPP
