#ifndef GUN_CLASS_HPP
#define GUN_CLASS_HPP

#include <iostream>
#include "Missile.class.hpp"
#include "Utils/Vector.class.hpp"

class Gun {

public:
	Gun(void);
	Gun(Gun const& src);
	~Gun(void);

	Utils::Vector<Missile*>&	getMissiles(void);

	inline int		getSpeed() const {
		return (this->_speed);
	}

	inline int 		getPower() const {
		return (this->_power);
	}

	inline int		getFireSpeed() const {
		return (this->_fire_speed);
	}

	inline Gun*		setPower(int power) {
		this->_power = power;
		return (this);
	}

	inline Gun*		setSpeed(int speed) {
		this->_speed = speed;
		return (this);
	}

	inline Gun*		setFireSpeed(int fire_speed) {
		this->_fire_speed = fire_speed;
		return (this);
	}

	Missile 		*newMissile(int x, int y, int xmax, int ymax);

	Gun& operator=(Gun const& rhs);
private:
	Utils::Vector<Missile*> _missiles;
	int						_speed;
	int						_power;
	int						_fire_speed;
};

std::ostream& operator<<(std::ostream& o, Gun const& i);

#endif //!GUN_CLASS_HPP
