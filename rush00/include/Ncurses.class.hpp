#ifndef NCURSES_CLASS_HPP
#define NCURSES_CLASS_HPP

#include <iostream>

class Ncurses {

public:
	Ncurses(void);
	~Ncurses(void);
	Ncurses(Ncurses const& src);
	void	Draw(std::string);
	void	Refresh(void);

	Ncurses& operator=(Ncurses const& rhs);

private:

};

#endif //!NCURSES_CLASS_HPP
