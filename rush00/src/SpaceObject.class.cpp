#include "SpaceObject.class.hpp"

SpaceObject::SpaceObject() : Object() , _power(0), _speed(0){}

SpaceObject::SpaceObject(SpaceObject const& src) : Object(src),
											_power(src._power), 
											_speed(src._speed){}

SpaceObject::SpaceObject(int power, int speed) : Object(), 
	_power(power),
	_speed(speed)
{}

SpaceObject::~SpaceObject() {}

int				SpaceObject::getPower(void) const { return this->_power; }
int				SpaceObject::getSpeed(void) const { return this->_speed; }

SpaceObject&	SpaceObject::operator=(SpaceObject const& rhs){
	Object::operator=(rhs);
	this->_power = rhs.getPower(); 
	this->_speed = rhs.getSpeed();
	return *this;
}

std::ostream & operator<<(std::ostream & o,  SpaceObject const& i){
	o << dynamic_cast<Object const&>(i)
	  << "Power : " << i.getPower() << std::endl 
	  << "speed : " << i.getSpeed() << std::endl;
	return (o);
}
