#include <Window.class.hpp>


Window::Window() : _x(0), _y(0), _win(NULL) {}
Window::Window(Window const& s) : _x(s._x), _y(s._y), _win(s._win) { *this = s; }

Window::~Window() {
	if (this->_win) {
		::delwin(this->_win);
		this->_win = NULL;
	}
}

void		Window::newWin(int height, int width, int y, int x, bool d) {
	(void)d;
	this->_win = newwin(height , width , y, x);
	this->_x = x; // here mais ca bug quand je change par = width
	this->_y = y; // here mais ca bug quand je change par = heigth
	this->_height = height;
	this->_width = width;
	if (!d) {
		this->_width -= 4;
	}
	box(this->_win, '|', '+');
}

void		Window::put(std::string const& life, std::string const& score) {
	if (life.size() + score.size() + 15 < XTOTAL) {
		::wclear(this->_win);
		mvwprintw(this->_win, 1, 6, life.c_str());
		mvwprintw(this->_win, 1, XTOTAL -2 - life.size() - 6 , score.c_str());
		box(this->_win, '|', '+');
		::wrefresh(this->_win);
	}
}

Window&	Window::operator=(Window const& rhs){
	if (this != &rhs) {
		this->_x = rhs._x; 
		this->_y = rhs._y; 
		this->_win = rhs._win; 
	}
	return *this;
}

std::ostream & operator<<(std::ostream & o, Window const& i){
	(void)o;
	(void)i;
	return (o);
}
