#include "Game.class.hpp"
#include "Collision.class.hpp"
#include "Missile.class.hpp"
#include "Player.class.hpp"
#include <sstream>
#include <cstdlib>
#include <ctime>

Game	Game::_singleton(25);

Game::Game() : Rotor(),
	_player(0),
	_time_place_flotte(0),
	_time_new_flotte(1000000)
{

}

Game::Game(int frames) : Rotor(frames),
	_player(0),
	_player_zone(0),
	_dis(0),
	_time_place_flotte(0),
	_time_new_flotte(1000000)
{
	struct termios ttystate;
	tcgetattr(STDIN_FILENO, &ttystate);
	// if (state == 1) {
	ttystate.c_lflag &= ~ICANON;
	ttystate.c_cc[VMIN] = 1;
	// }
	// else if (state == 0)
	// 	ttystate.c_lflag |= ICANON;
	tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

Game::Game(const Game& g) :
	Rotor(g),
	_player(g._player),
	_time_place_flotte(0),
	_time_new_flotte(1000000)
{

}

Game& Game::operator=(const Game& g)
{
	Rotor::operator=(g);
	this->_player = g._player;
	return (*this);
}

Game::~Game()
{
	// if ((unsigned int)this->_player->getScore() > this->_player->getMaxScore()) {
	// 	std::ofstream	ofs(this->_player->getName()+".score");
	// 	if (ofs.is_open()) {
	// 		ofs << this->_player->getMaxScore();
	// 		ofs.close();
	// 	}
	// }
	delete this->_player;
	delete this->_dis;
}

void	Game::onFrame()
{
	this->_manageKey();
	this->_checkEnemies();
	this->_checkPlayerShoots();
	this->_recalculate_pos();
	this->_recalculate_pos_player();
	if (this->getTime(this->getNbFrame()) - this->_time_place_flotte > this->_time_new_flotte)
		this->_placeEnemies();
	std::stringstream score;
	score << this->_player->getScore();
	std::stringstream life;
	life << this->_player->getLife();
	this->_dis->putUp(life.str(), score.str());
}

int		Game::_getFreeX() {
	return this->getDisplay()->getWidth();
}

int		Game::_getFreeY() {
	static bool	init = false;
	int			height = this->getDisplay()->getHeigth();
	int			rand;

	if (!init && (init = true))
		std::srand(std::time(0));
	rand = std::rand();
	return rand % (height - 5);
}

void	Game::_manageKey() {
	char		c;
	while ((c = this->_key.update())) {
		// if (c)
	// 	std::cout << c << std::endl;
		if (c == 'w') {
			--(*this->_player);
		} 
		else if (c == 's') {
			++(*this->_player);
		}
		else if (c == ' ')
			this->_shootPlayer();
	}
}

void	Game::_placeEnemies() {
	int		x = this->_getFreeX();
	int		y = this->_getFreeY();

	Gun			*g = new Gun();
	g->setSpeed(5000);
	g->setPower(10);
	g->setFireSpeed(1000000);
	SpaceShip		*cur = new SpaceShip(100);
	cur->setPower(30);
	cur->setSpeed(10);
	cur->setX(x);
	cur->setY(y);
	cur->setX_max(x + 2);
	cur->setY_max(y + 2);
	cur->setGun(g);
	this->_objects.push(cur);
	this->_object_start_x[cur] = cur->getX();
	this->_object_frame[cur] = this->getTime(this->getNbFrame());
	this->_last_fire[cur->getGun()] = this->getTime(this->getNbFrame());
	this->_time_new_flotte -= 1000;
	this->_time_place_flotte = this->getTime(this->getNbFrame());
}

void	Game::_checkEnemies() {
	for (uint32_t i = 0;i < this->_objects.size(); ++i) {
		this->_dis->display(this->_objects[i]);
		SpaceShip *ship = dynamic_cast<SpaceShip*>(this->_objects[i]);
		if (ship && ship->getGun()) {
			for (uint32_t j = 0; j < ship->getGun()->getMissiles().size(); ++j) {
				this->_dis->display(ship->getGun()->getMissiles()[j]);
			}
		}
	}
	this->_dis->display(this->_player);
}

void	Game::_checkPlayerShoots() {
	if (this->_player->getGun()) {
		for (uint32_t i = 0; i < this->_player->getGun()->getMissiles().size(); ++i) {
			this->_recalculate(this->_player->getGun()->getMissiles()[i], true);
			if (this->_player->getGun()->getMissiles()[i]->getX() < this->_dis->getWidth())
				this->_dis->display(this->_player->getGun()->getMissiles()[i]);
			else
			{
				Missile *m = this->_player->getGun()->getMissiles()[i];
				m->getParent()->getMissiles().erase(m);
				this->_object_frame.erase(m);
				this->_object_start_x.erase(m);
				this->_dis->undisplay(m);
			}
		}
	}
}

void	Game::setPlayer(const std::string& name) {
	this->_player = new Player(name);
	this->_player->setMaxY(this->_dis->getHeigth());
}

bool	Game::_is_alive(const SpaceShip* obj) {
	if (obj->getLife() <= 0)
		return (true);
	return (false);
}

void	Game::_shootPlayer()
{
	if (this->_player && this->_player->getGun()) {
		if (this->_canShoot(this->_player->getGun())) {
			std::cout << "in" << std::endl;
			Missile *m = this->_player->getGun()->newMissile(this->_player->getX() + 4, this->_player->getY(), this->_player->getX_max() + 4, this->_player->getY_max());
			this->_object_frame[m] = this->getTime(this->getNbFrame());
			this->_object_start_x[m] = m->getX();
		}
	}
}

void	Game::_recalculate(SpaceObject *obj, bool player_missile)
{
	uint32_t width = obj->getX_max() - obj->getX();
	unsigned int diff_time = this->getTime(this->getNbFrame()) - this->_object_frame[obj];
	int pos_x;
	if (!player_missile)
		pos_x = ((float)this->_object_start_x[obj]) - (diff_time / (10000 - obj->getSpeed()) * 0.1);
	else
		pos_x = ((float)this->_object_start_x[obj]) + (diff_time / (10000 - obj->getSpeed()) * 0.1);
	obj->setX(pos_x);
	obj->setX_max(pos_x + width);	
}

void	Game::_recalculate_pos_player()
{
	if (this->_player->getGun()) {
		for (uint32_t i = 0; i < this->_player->getGun()->getMissiles().size(); ++i) {
			Missile *m = this->_player->getGun()->getMissiles()[i];
			bool destroy = false;
			for (uint32_t j = 0;j < this->_objects.size(); ++j) {
				if (Collision::has(*this->_objects[j], *m)) {
					SpaceShip *ship = dynamic_cast<SpaceShip*>(this->_objects[j]);
					if (ship)
					{
						ship->setLife(ship->getLife() - m->getPower());
						if (ship->getLife() <= 0) {
							if (ship && ship->getGun()) {
								for (uint32_t j = 0; j < ship->getGun()->getMissiles().size(); ++j)
								{
									Missile *m = ship->getGun()->getMissiles()[j];
									this->_object_frame.erase(m);
									this->_object_start_x.erase(m);
									this->_dis->undisplay(m);
								}
								this->_last_fire.erase(ship->getGun());
							}
							this->_dis->undisplay(ship);
							this->_objects.erase(ship);
							delete ship;
							this->_player->setScore(this->_player->getScore() + 10);
						}
					}
					destroy = true;
					break ;
				}
			}
			if (destroy)
			{
				m->getParent()->getMissiles().erase(m);
				this->_object_frame.erase(m);
				this->_object_start_x.erase(m);
				this->_dis->undisplay(m);
				delete m;
			}
		}
	}	
}

bool	Game::_tu_te_demerde (SpaceObject *skip) {
	int		life;
	if (Collision::has(*this->_player, *skip)) {
		life = this->_player->getLife() - skip->getPower();
		if (life <= 0) {
			return (false);
		} else
			this->_player->setLife(life);
	}
	return (true);
}

void	Game::_recalculate_pos()
{
	for (uint32_t i = 0; i < this->_objects.size(); ++i) {
		this->_recalculate(this->_objects[i]);
		if (!this->_tu_te_demerde(this->_objects[i])) {
			this->setEnd(true);
			break ;
		}
		if(Collision::has(*this->_player_zone, *this->_objects[i]) || this->_objects[i]->getX() < 0) {
			SpaceObject *obj = this->_objects[i];
			this->_objects.erase(obj);
			this->_object_frame.erase(obj);
			this->_object_start_x.erase(obj);
			SpaceShip *ship = dynamic_cast<SpaceShip*>(obj);
			if (ship && ship->getGun()) {
				for (uint32_t j = 0; j < ship->getGun()->getMissiles().size(); ++j)
				{
					Missile *m = ship->getGun()->getMissiles()[j];
					this->_object_frame.erase(m);
					this->_object_start_x.erase(m);
					this->_dis->undisplay(m);
				}
				this->_last_fire.erase(ship->getGun());
			}
			this->_dis->undisplay(obj);
			delete obj;
		}
		SpaceShip *ship = dynamic_cast<SpaceShip*>(this->_objects[i]);
		if (ship && ship->getGun())
		{
			Utils::Vector<Missile*>&	missiles = ship->getGun()->getMissiles();
			for (uint32_t i = 0;i < missiles.size(); ++i) {
				this->_recalculate(missiles[i]);
				if (this->getEnd() || !this->_tu_te_demerde(missiles[i])) {
					this->setEnd(true);
					return  ;
				}
				bool b = false;
				for (uint32_t k = 0;k < this->_objects.size(); ++k) {
					if (this->_objects[k] != ship) {
						if (Collision::has(*missiles[i], *this->_objects[k])) {
							Missile *m = missiles[i];
							m->getParent()->getMissiles().erase(m);
							this->_object_frame.erase(m);
							this->_object_start_x.erase(m);
							this->_dis->undisplay(m);
							delete m;
							b = true;
							break ;
						}
					}
				}
				if(!b && (Collision::has(*missiles[i], *this->_player_zone) || missiles[i]->getX() < 0))
				{
					Missile *m = missiles[i];
					m->getParent()->getMissiles().erase(m);
					this->_object_frame.erase(m);
					this->_object_start_x.erase(m);
					this->_dis->undisplay(m);
					delete m;
				}
			}
			if (this->_canShoot(ship->getGun())) {
				Missile *m = ship->getGun()->newMissile(ship->getX(), ship->getY(), ship->getX_max(), ship->getY_max());
				this->_object_frame[m] = this->getTime(this->getNbFrame());
				this->_object_start_x[m] = m->getX();
			}
		}
	}
}

bool	Game::_canShoot(Gun *gun) {
	clock_t		current_time = this->getTime(this->getNbFrame());
	clock_t		last_time = this->_last_fire[gun];
	bool		ret = (current_time - last_time >= (clock_t)gun->getFireSpeed());

	if (ret) {
		this->_last_fire[gun] = current_time;
		return (true);
	}
	return (false);
}

void	Game::_check_alives()
{
	for (uint32_t i = 0; i < this->_objects.size(); ++i) {
		Object *obj = this->_objects[i];
		SpaceShip *ship = dynamic_cast<SpaceShip*>(obj);
		if (ship && !this->_is_alive(ship)) {
			// 
		}
	}
}

void	Game::init(const std::string& player_name)
{
	Gun *g = new Gun();
	g->setPower(45);
	g->setSpeed(5000);
	g->setFireSpeed(10000);
	this->_last_fire[g] = this->getTime(this->getNbFrame());
	this->_dis = new Display();
	this->_dis->putUp("50", "400");
	// std::stringstream maxscore;
	// maxscore << max_score;
	this->_dis->putDown(player_name, "");
	//this->_dis->refresh();
	this->setPlayer(player_name);
	// this->_player->setMaxScore(max_score);
	this->_player_zone = new Object(0, 0, 2, this->_dis->getHeigth());
	this->_player->setX(0);
	this->_player->setGun(g);
	this->_player->setX_max(2);
	this->_player->setY((this->_dis->getHeigth() - 2) / 2 - 1);
	this->_player->setMaxY(this->_dis->getHeigth());
	this->_player->setY_max((this->_dis->getHeigth() - 2) / 2 + 1);
}
