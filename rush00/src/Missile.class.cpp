#include "Missile.class.hpp"
#include "Gun.class.hpp"

Missile::Missile() : SpaceObject() {
	return;
}

Missile::Missile(Gun *parent) :
	SpaceObject(),
	_parent(parent)
{
	// std::cout << "Create missile: " << this << std::endl;
}

Missile::Missile(Missile const& src) : SpaceObject(),
	_parent(src._parent) {
	(void)src;
	return;
}

Missile::~Missile() {
	// std::cout << "Destroy missile: " << this << std::endl;
}

Missile&	Missile::operator=(Missile const& rhs){
	SpaceObject::operator=(rhs);
	this->_parent = rhs._parent;
	return *this;
}

Gun 		*Missile::getParent()
{
	return (this->_parent);
}
