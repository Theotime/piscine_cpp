#include "Collision.class.hpp"

Collision::Collision() {

}

Collision::~Collision() {

}

Collision::Collision(const Collision& c) {
	(void)c;
}

Collision& Collision::operator=(const Collision& c) {
	(void)c;
	return (*this);
}

bool	Collision::has(const Object& o1, const Object& o2)
{
	if ((o2.getX() >= o1.getX() && o2.getX() <= o1.getX_max())
		|| (o2.getX_max() >= o1.getX() && o2.getX_max() <= o1.getX_max()))
	{
		if ((o2.getY() >= o1.getY() && o2.getY() <= o1.getY_max())
			|| (o2.getY_max() >= o1.getY() && o2.getY_max() <= o1.getY_max()))
			return (true);
	}
	return (false);
}