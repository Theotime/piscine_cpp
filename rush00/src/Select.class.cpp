#include "Select.class.hpp"
#include <fstream>
#define STDIN 0
#include <iostream>
#include <ncurses.h>

Select::Select() {
	return;
}

Select::Select(Select const& src) {
	*this = src;
	return;
}

Select::~Select() {
	return;
}

char		Select::update() {
	/*static std::ofstream	ofs("log.txt");
	ofs.open("log.txt", std::ios::app);
	ofs << "in ";
	struct timeval tv;
	tv.tv_sec = 0;
	tv.tv_usec = 100;
	FD_ZERO(&this->_fds);
	FD_SET(0, &this->_fds);
	int ret = select(1, &this->_fds, NULL, NULL, &tv);
	ofs << ret << " ";
	ofs.close();
	if (FD_ISSET(0, &this->_fds)) {
		char c;
		read(0, &c, 1);
		std::cout << "c: " << (int)c << std::endl;
		if (c == ' ')
			std::cout << "space" << std::endl;
		return (c);
	}*/
	char	c = getch();
	return ((c == ERR) ? 0 : c);
}

Select&	Select::operator=(Select const& rhs){
	this->_fds = rhs._fds;
	return *this;
}
