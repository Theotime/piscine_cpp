#include "SpaceShip.class.hpp"
#include "SpaceObject.class.hpp"

SpaceShip::SpaceShip() : SpaceObject(), _life(0), _gun(0) {
	return;
}

SpaceShip::SpaceShip(int life) : SpaceObject(),
	  _life(life), _gun(0)
{
	// std::cout << "Create Ship: " << this << std::endl;
}

SpaceShip::SpaceShip(SpaceShip const& src) : SpaceObject(src),
	_life(src._life), _gun(src._gun)
{
}

SpaceShip::~SpaceShip() {
	if (this->_gun)
		delete this->_gun;
	// std::cout << "Delete Ship: " << this << std::endl;
}

int	SpaceShip::getLife(void) const { return this->_life; }

SpaceShip&	SpaceShip::operator=(SpaceShip const& rhs){
	SpaceObject::operator=(rhs);
	this->_life = rhs.getLife();
	this->_gun = rhs._gun;
	return *this;
}

std::ostream & operator<<(std::ostream & o, SpaceShip const& i){
	o << dynamic_cast<SpaceObject const&>(i)
	  << "Life :" << i.getLife() << std::endl;
	return o;
}
