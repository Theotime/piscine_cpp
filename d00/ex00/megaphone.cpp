/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 12:36:50 by triviere          #+#    #+#             */
/*   Updated: 2015/01/05 12:38:07 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>


void		ft_upper(std::string *str)
{
	for (unsigned int i = 0; i < str->length(); ++i)
	{
		if ((int) (*str)[i] > 96 && (int) (*str)[i] < 123)
		{
			(*str)[i] = (char) ((int) (*str)[i] - 32);
		}
	}
}

void		ft_concat_args(std::string *dest, char **av, int ac)
{
	for (int i = 1; i < ac; ++i)
		*dest += av[i];
}

int		main (int ac, char **av)
{
	std::string		result;
	if (ac < 2)
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
		return (1);
	}
	ft_concat_args(&result, av, ac);
	ft_upper(&result);
	std::cout << result << std::endl;
	return (0);
}
