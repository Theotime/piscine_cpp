/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 16:36:43 by triviere          #+#    #+#             */
/*   Updated: 2015/01/05 22:59:04 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.hpp"



int		main() {
	bool			end = false;
	Contacts		contacts;
	std::string		cmd;

	while (!end) {
		std::cout << "$--> ";
		std::getline(std::cin, cmd);
		if (!cmd.compare("EXIT"))
			break ;
		else if (!cmd.compare("ADD"))
			contacts.add();
		else if (!cmd.compare("SEARCH"))
			contacts.search();
	}
	return (0);
}
