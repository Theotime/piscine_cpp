/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 16:36:41 by triviere          #+#    #+#             */
/*   Updated: 2015/01/05 21:05:56 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __HEADER_HPP__
# define __HEADER_HPP__

# include <iostream>
# include <iomanip>
# include <sstream>
# include <string>
# include <ctime>

# include "user.class.hpp"
# include "contacts.class.hpp"

#endif
