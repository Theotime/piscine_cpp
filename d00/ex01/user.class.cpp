/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user.class.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 17:03:44 by triviere          #+#    #+#             */
/*   Updated: 2015/01/05 22:41:18 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.hpp"


User::User() :
	_actif(false)
{
}

User::~User() {
}

std::string		User::getFirstName()	{ return this->_first_name;		}
std::string		User::getLastName()		{ return this->_last_name;		}
std::string		User::getNickname()		{ return this->_nickname;		}
std::string		User::getLogin()		{ return this->_login;			}
std::string		User::getMail()			{ return this->_mail;			}
std::string		User::getNumber()		{ return this->_number;			}
std::string		User::getFood()			{ return this->_food;			}
std::string		User::getPentiesColor()	{ return this->_pentise_color;	}
std::string		User::getBirthday()		{ return this->_birthday;		}
std::string		User::getCp()			{ return this->_cp;				}
std::string		User::getSecret()		{ return this->_secret;			}
bool			User::getActif()		{ return this->_actif;			}


User			*User::setFirstName(std::string first_name) {
	this->_first_name = first_name;
	return (this);
}

User			*User::setActif(bool actif) {
	this->_actif = actif;
	return (this);
}

User			*User::setLastName(std::string last_name) {
	this->_last_name = last_name;
	return (this);
}

User			*User::setNickname(std::string nickname) {
	this->_nickname = nickname;
	return (this);
}

User			*User::setLogin(std::string login) {
	this->_login = login;
	return (this);
}

User			*User::setMail(std::string mail) {
	this->_mail = mail;
	return (this);
}

User			*User::setNumber(std::string number) {
	this->_number = number;
	return (this);
}

User			*User::setFood(std::string food) {
	this->_food = food;
	return (this);
}

User			*User::setPentiesColor(std::string penties_color) {
	this->_pentise_color = penties_color;
	return (this);
}

User			*User::setBirthday(std::string birthday) {
	this->_birthday = birthday;
	return this;
}

User			*User::setCp(std::string cp) {
	this->_cp = cp;
	return (this);
}

User			*User::setSecret(std::string secret) {
	this->_secret = secret;
	return (this);
}

void			User::display(int index) {
	std::stringstream	ss;
	std::string			n;
	if (this->getActif()) {
		ss << index;
		n = ss.str();
		this->print(n);
		this->print(this->getFirstName());
		this->print(this->getLastName());
		this->print(this->getNickname());
		std::cout << std::endl;
	}
}

void			User::displayAll() {
	if (this->getActif()) {
		std::cout << "First Name : " << this->getFirstName() << std::endl;
		std::cout << "Last Name : " << this->getLastName() << std::endl;
		std::cout << "Nickname : " << this->getNickname() << std::endl;
		std::cout << "Login : " << this->getLogin() << std::endl;
		std::cout << "Postal : " << this->getCp() << std::endl;
		std::cout << "Email Address : " << this->getMail() << std::endl;
		std::cout << "Phone Number : " << this->getNumber() << std::endl;
		std::cout << "Birthday date : " << this->getBirthday() << std::endl;
		std::cout << "Favorite meal : " << this->getFood() << std::endl;
		std::cout << "Underwear color : " << this->getPentiesColor() << std::endl;
		std::cout << "Darkest secret : " << this->getSecret() << std::endl;
	}
}

void			User::print(std::string str) {
	if (str.length() > 10)
		str = str.substr(0, 9) + ".";
	std::cout << std::setw(11) << std::right;
	std::cout << str << "|";
}
