/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contacts.class.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 19:14:24 by triviere          #+#    #+#             */
/*   Updated: 2015/01/05 22:39:48 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __CONTACTS_CLASS_HPP__
# define __CONTACTS_CLASS_HPP__

class Contacts {
	public:
		Contacts();
		~Contacts();

		bool			add();
		bool			addUser(int index);
		void			search();
		void			displayUsers();
		void			displayAll();
		int				getEmpty();
		int				selectUser(const char *str);
		std::string		getResponse(const char *str);

	private:
		User			_users[8];
		int				_nb;
};

#endif
