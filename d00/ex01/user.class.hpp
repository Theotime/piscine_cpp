/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user.class.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 17:03:11 by triviere          #+#    #+#             */
/*   Updated: 2015/01/05 22:40:10 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#ifndef __USER_CLASS_HPP__
# define __USER_CLASS_HPP__

class User {

	public:
		User();
		~User();

		std::string		getFirstName();
		std::string		getLastName();
		std::string		getNickname();
		std::string		getLogin();
		std::string		getMail();
		std::string		getNumber();
		std::string		getFood();
		std::string		getPentiesColor();
		std::string		getBirthday();
		std::string		getCp();
		std::string		getSecret();
		bool			getActif();

		User			*setFirstName(std::string first_name);
		User			*setLastName(std::string last_name);
		User			*setNickname(std::string nickname);
		User			*setLogin(std::string login);
		User			*setMail(std::string mail);
		User			*setNumber(std::string number);
		User			*setFood(std::string food);
		User			*setPentiesColor(std::string penties_color);
		User			*setBirthday(std::string birthday);
		User			*setCp(std::string cp);
		User			*setSecret(std::string secret);
		User			*setActif(bool actif);

		void			display(int index);
		void			print(std::string str);
		void			displayAll();

	private:
		std::string		_first_name;
		std::string		_last_name;
		std::string		_nickname;
		std::string		_login;
		std::string		_mail;
		std::string		_number;
		std::string		_food;
		std::string		_pentise_color;
		std::string		_birthday;
		std::string		_cp;
		std::string		_secret;
		bool			_actif;
};

#endif
