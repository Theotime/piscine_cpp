/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   contacts.class.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 19:18:59 by triviere          #+#    #+#             */
/*   Updated: 2015/01/05 23:05:45 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.hpp"

Contacts::Contacts():
	_nb(0)
{
	
}

Contacts::~Contacts() {
}

int				Contacts::getEmpty() {
	for (int i = 0; i < 8; ++i) {
		if (!this->_users[i].getActif())
			return (i);
	}
	return -1;
}

int				Contacts::selectUser(const char *str) {
	bool				end = !(bool) this->_nb;
	int					i;
	std::string			n;

	while (!end) {
		std::stringstream	ss;
		this->displayUsers();
		n = this->getResponse(str);
		if (n.length() > 1) {
			continue ;
		}
		ss << n;
		ss >> i;
		if (i < 10 && i > 0 && this->_users[i - 1].getActif())
			end = true;
	}
	return (i - 1);
}

std::string		Contacts::getResponse(const char *str) {
	std::string		tmp;
	bool			empty = true;

	while (empty) {
		std::cout << str << " : ";
		std::getline(std::cin, tmp);
		empty = !(bool) tmp.length();
	}
	return (tmp);
}

bool			Contacts::addUser(int index) {
	User		*cur;

	cur = &(this->_users[index]);
	cur->setFirstName(this->getResponse("First Name"));
	cur->setLastName(this->getResponse("Last Name"));
	cur->setNickname(this->getResponse("Nickname"));
	cur->setLogin(this->getResponse("Login"));
	cur->setCp(this->getResponse("Postal"));
	cur->setMail(this->getResponse("Email address"));
	cur->setNumber(this->getResponse("Phone number"));
	cur->setBirthday(this->getResponse("Birthday date"));
	cur->setFood(this->getResponse("Favorite meal"));
	cur->setPentiesColor(this->getResponse("Underwear color"));
	cur->setSecret(this->getResponse("Darkest secret"));
	cur->setActif(true);
	this->_nb++;
	return (true);
}

bool			Contacts::add() {
	int		index;

	index = this->getEmpty();
	if (index == -1)
		index = this->selectUser("PLEASE SELECT USER ID FOR REPLACE");
	this->addUser(index);
	return (true);
}

void			Contacts::search() {
	int		n;

	if (this->_nb > 0) {
		n = this->selectUser("WHO YOU SEARCH");
		this->_users[n].displayAll();
	} else {
		std::cout << "NO BODY IN YOUR CONTACT" << std::endl;
	}
}

void			Contacts::displayUsers() {
	for (int i = 0; i < 8; ++i) {
		this->_users[i].display(i + 1);
	}
}
