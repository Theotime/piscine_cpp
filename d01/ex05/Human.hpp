/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 07:53:03 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 08:25:16 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __HUMAN_HPP__
# define __HUMAN_HPP__

# include <string>
# include <iostream>
# include "Brain.hpp"

class Human {

	public:
		Human();
		~Human();

		Brain			&getBrain();
		std::string		identify() const;

	private:
		Brain		_brain;
};

#endif
