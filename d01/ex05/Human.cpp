/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 07:53:07 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 08:25:33 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.hpp"
#include "Brain.hpp"

Human::Human() { }

Human::~Human() { }

Brain			&Human::getBrain() {
	Brain		&b = this->_brain;

	return (b);
};

std::string		Human::identify() const {
	return (this->_brain.identify());
}
