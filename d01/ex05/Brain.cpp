/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 07:53:20 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 08:21:52 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.hpp"

Brain::Brain() {

}

Brain::~Brain() {

}

std::string			Brain::identify() const {
	std::stringstream		ss;
	std::string				res;

	ss << this;
	ss >> res;
	return res;
}
