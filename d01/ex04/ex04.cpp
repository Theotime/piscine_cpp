/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 07:19:53 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 07:25:20 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <string>

int			main () {
	std::string		brain = "HI THIS IS BRAIN";
	std::string		&ref_brain = brain;
	std::string		*ptr_brain = &brain;

	std::cout << ref_brain << std::endl;
	std::cout << *ptr_brain << std::endl;
	return (0);
}
