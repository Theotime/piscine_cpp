/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 05:36:44 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 06:43:34 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"

int			main () {
	ZombieEvent		manager;
	Zombie			*tmp;

	for (int i = 0; i < 20; ++i) {
		tmp = manager.randomChump();
		tmp->announce();
		delete tmp;
	}

	return (0);
}
