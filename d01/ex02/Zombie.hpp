/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 05:37:07 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 06:34:12 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __ZOMBIE_HPP__
# define __ZOMBIE_HPP__

# include <string>
# include <iostream>
# include <ctime>
# include <cstdlib>

class Zombie {
	public:
		Zombie(std::string name, int type);
		~Zombie();

		std::string		getName() const;
		int				getType() const;

		Zombie			*setName(std::string name);
		Zombie			*setType(int type);

		void			announce();

	private:
		std::string		_name;
		int				_type;
};

#endif
