/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 05:37:19 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 06:27:18 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __ZOMBIE_EVENT_HPP__
# define __ZOMBIE_EVENT_HPP__

class ZombieEvent {
	public:
		ZombieEvent();
		~ZombieEvent();

		int			getZombieType() const;

		Zombie		*newZombie(std::string);
		ZombieEvent	*setZombieType(int type);

		Zombie		*randomChump();

	private:
		int			_zombie_type;

};

#endif
