/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 05:37:13 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 06:40:10 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent():
	_zombie_type(0)
{

}

ZombieEvent::~ZombieEvent() {

}

int					ZombieEvent::getZombieType() const { return (this->_zombie_type); }

ZombieEvent			*ZombieEvent::setZombieType(int type) {
	this->_zombie_type = type;
	return (this);
}

Zombie				*ZombieEvent::newZombie(std::string name) {
	return new Zombie(name, this->getZombieType());
}

Zombie				*ZombieEvent::randomChump() {
	std::string		random_name;
	static bool 	init = false;

	if (!init && (init = true))
		std::srand((time_t) this);
	for (int i = 0; i < 42; ++i) {
		random_name += (char) (std::rand() % 26 + 97);
	}
	return newZombie(random_name);
}
