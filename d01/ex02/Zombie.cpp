/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 05:37:00 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 06:43:55 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "Zombie.hpp"
#include "ZombieEvent.hpp"

Zombie::Zombie(std::string name, int type):
	_name(name),
	_type(type)
{

}

Zombie::~Zombie() {

}

std::string		Zombie::getName() const { return this->_name; }
int				Zombie::getType() const { return this->_type; }

Zombie			*Zombie::setName(std::string name) {
	this->_name = name;
	return (this);
}

Zombie			*Zombie::setType(int type) {
	this->_type = type;
	return (this);
}

void			Zombie::announce() {
	std::cout << "<" << this->getName() << " (" << this->getType() << ")>  Braiiiiiiinnnssss..." << std::endl;
}
