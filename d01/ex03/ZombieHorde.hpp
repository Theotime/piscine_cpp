/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 06:48:18 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 07:10:36 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __ZOMBIE_EVENT_HPP__
# define __ZOMBIE_EVENT_HPP__

class ZombieHorde {
	public:
		ZombieHorde(int n);
		~ZombieHorde();

		std::string		randomChump();

		void			announce();

	private:
		Zombie			*_zombies;
		int				_length;

};

#endif
