/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 06:48:58 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 07:10:12 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.hpp"
#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n):
	_length(n)
{
	this->_zombies = new Zombie[n];
	for (int i = 0; i < n; ++i) {
		this->_zombies[i].setName(this->randomChump());
	}
}

ZombieHorde::~ZombieHorde() {
	delete[] this->_zombies;
}

void		ZombieHorde::announce() {
	for (int i = 0; i < this->_length; ++i) {
		this->_zombies[i].announce();
	}
}

std::string			ZombieHorde::randomChump() {
	std::string		random_name;
	static bool 	init = false;

	if (!init && (init = true))
		std::srand((time_t) this);
	for (int i = 0; i < 42; ++i) {
		random_name += (char) (std::rand() % 26 + 97);
	}
	return random_name;
}


