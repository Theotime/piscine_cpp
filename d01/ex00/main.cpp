/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 04:42:15 by triviere          #+#    #+#             */
/*   Updated: 2015/01/07 04:58:03 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void		ponyOnTheStack() {
	Pony		stack;
}

void		ponyOnTheHeap() {
	Pony		*heap;

	heap = new Pony();
	delete (heap);
}

int			main () {
	ponyOnTheHeap();
	ponyOnTheStack();
	return (0);
}

