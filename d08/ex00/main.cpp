/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/16 01:37:01 by triviere          #+#    #+#             */
/*   Updated: 2015/01/16 03:33:18 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.hpp"

int		main() {
	std::list<int>	l;
	int				res;

	l.push_back(1);
	l.push_back(2);
	l.push_back(3);
	l.push_back(4);
	l.push_back(5);
	l.push_back(6);
	l.push_back(7);
	l.push_back(8);
	l.push_back(9);
	l.push_back(10);

	try {
		res = easyfind(l, 13);
		std::cout << res << std::endl;
	} catch (std::string &caugth) {
		std::cout << caugth << std::endl;
	}

	return (0);
}

