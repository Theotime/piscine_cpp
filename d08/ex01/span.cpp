/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/16 03:41:42 by triviere          #+#    #+#             */
/*   Updated: 2015/01/16 06:46:44 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "span.hpp"

Span::Span():
	_n(0),
	_cur(0)
{

}

Span::Span(unsigned int n):
	_n(n),
	_cur(0)
{

}

Span::Span(Span const &o) {
	*this = o;
}

Span::~Span() {

}

void				Span::addNumber(int const &n) {
	if (this->_n > this->_cur) {
		this->_all.push_back(n);
		this->_all.sort();
		++this->_cur;
	} else
		throw std::string("T'es con ou quoi ?");
}

int					Span::shortestSpan() {
	std::list<int>::const_iterator	it = _all.begin();
	std::list<int>::const_iterator	it2 = ++it;
	std::list<int>::const_iterator	end = _all.end();
	int								tmp;

	for (it = _all.begin(); it != --end; ++it) {
		if (it == _all.begin() || tmp > (*it2 - *it))
			tmp = *it2 - *it;
		if (it2 != _all.end())
			++it2;
	}
	return (tmp);
}

int					Span::longestSpan() {
	std::list<int>::const_iterator	it = _all.begin();
	std::list<int>::const_iterator	ite = --_all.end();
	
	return (*ite - *it);
}

void				Span::display() {
	std::list<int>::const_iterator		it;

	for (it = this->_all.begin(); it != this->_all.end(); ++it) {
		std::cout << *it << std::endl;
	}
}

Span				&Span::operator=(Span const &o) {
	this->_n = o._n;
	this->_cur = o._cur;
	this->_all = o._all;
	return (*this);
}
