/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 06:32:17 by triviere          #+#    #+#             */
/*   Updated: 2015/01/09 06:35:53 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap(const std::string name): ClapTrap(60, 60 , 120, 120, 1, 60, 5, name) {
	std::cout << "Create Ninja : " << this->getName()<< std::endl;
}

NinjaTrap::~NinjaTrap() {
	std::cout << "Destruct NinjaTrap : " << this->getName() << std::endl;
}