/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 07:54:13 by triviere          #+#    #+#             */
/*   Updated: 2015/01/09 07:57:24 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __NINJATRAP_CPP__
# define __NINJATRAP_CPP__

class NinjaTrap {
	public:
		NinjaTrap(const std::string name);
		~NinjaTrap();
		void		ninjaShoebox(std::string const &target);
};

#endif
