/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:13 by triviere          #+#    #+#             */
/*   Updated: 2015/01/09 04:24:17 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FRAGTRAP_HPP__
# define __FRAGTRAP_HPP__

# include "ClapTrap.hpp"
# include <string>
# include <iostream>
# include <ctime>
# include <cstdlib>

class FragTrap : public ClapTrap {

	public:
		FragTrap(const std::string name);
		~FragTrap();

		void		vaulthunter_dot_exe(std::string const & target);
};

#endif
