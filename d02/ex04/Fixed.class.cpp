/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 20:09:15 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 06:34:10 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.hpp"

Fixed::Fixed():
	_bits(FRACTIONAL_BITS)
{
	this->setRawBits(0);
}

Fixed::Fixed(Fixed const &obj):
	_bits(FRACTIONAL_BITS)
{
	*this = obj;
}

Fixed::Fixed(int value):
	_bits(FRACTIONAL_BITS)
{
	this->setRawBits(roundf(value * std::pow(2, this->_bits)));
}

Fixed::Fixed(float value):
	_bits(FRACTIONAL_BITS)
{
	this->setRawBits(roundf(value * std::pow(2, this->_bits)));
}

Fixed::~Fixed() {
}

void			Fixed::setRawBits(int const raw) {
	this->_value = raw;
}

int				Fixed::getRawBits()						const {
	return (this->_value);
}

int				Fixed::toInt()							const {
	return this->getRawBits() / std::pow(2, this->_bits);
}

float			Fixed::toFloat()						const {
	return (float)this->getRawBits() / std::pow(2, this->_bits);
}

const Fixed		&Fixed::max(Fixed const &a, Fixed const &b) {
	return (a >= b ? a : b);
}

const Fixed		&Fixed::min(Fixed const &a, Fixed const &b) {
	return (a <= b ? a : b);
}

/**
 * OPPERATOR
 **/

Fixed			&Fixed::operator=(Fixed const &obj) {
	this->_value = obj.getRawBits();
	return *this;
}

bool			Fixed::operator<(Fixed const &obj)		const {
	return (this->getRawBits() < obj.getRawBits());
}

bool			Fixed::operator>(Fixed const &obj)		const {
	return (this->getRawBits() > obj.getRawBits());
}

bool			Fixed::operator<=(Fixed const &obj)		const {
	return (this->getRawBits() <= obj.getRawBits());
}

bool			Fixed::operator>=(Fixed const &obj)		const {
	return (this->getRawBits() >= obj.getRawBits());
}

bool			Fixed::operator==(Fixed const &obj)		const {
	return (this->getRawBits() == obj.getRawBits());
}

bool			Fixed::operator!=(Fixed const &obj)		const {
	return (this->getRawBits() != obj.getRawBits());
}

Fixed			Fixed::operator+(Fixed const &obj)		const {
	return (this->toFloat() + obj.toFloat());
}

Fixed			Fixed::operator-(Fixed const &obj)		const {
	return (this->toFloat() - obj.toFloat());
}

Fixed			Fixed::operator*(Fixed const &obj)		const {
	return (this->toFloat() * obj.toFloat());
}

Fixed			Fixed::operator/(Fixed const &obj)		const {
	return (this->toFloat() / obj.toFloat());
}

Fixed			&Fixed::operator++() {
	this->_value++;
	return (*this);
}

Fixed			&Fixed::operator++(int) {
	Fixed		*tmp = new Fixed(*this);
	this->_value++;
	return (*tmp);
}

Fixed			&Fixed::operator--() {
	this->_value--;
	return (*this);
}

Fixed			&Fixed::operator--(int) {
	Fixed		*tmp = new Fixed(*this);

	this->_value--;
	return (*tmp);
}

std::ostream		&operator<<(std::ostream &o, Fixed const &src) {
	o << src.toFloat();
	return (o);
}

