/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Calculator.class.hpp                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 06:26:24 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 06:28:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __CALCULATOR_HPP__
# define __CALCULATOR_HPP__

class Calculator {
	public:
		Calculator(char *str);
		~Calculator();

	private:

};

#endif
