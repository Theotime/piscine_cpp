/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 20:09:12 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 06:39:29 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FIXED_CLASS_HPP__
# define __FIXED_CLASS_HPP__

# define FRACTIONAL_BITS 8

# include <string>
# include <iostream>
# include <cmath>

class Fixed {

	public:
		Fixed();
		Fixed(Fixed const &obj);
		Fixed(int value);
		Fixed(float value);
		~Fixed();

		int						getRawBits()					const;

		void					setRawBits(int const raw);

		static const Fixed		&max(Fixed const &a, Fixed const &b);
		static const Fixed		&min(Fixed const &a, Fixed const &b);
		static Fixed			&max(Fixed &a, Fixed &b);
		static Fixed			&min(Fixed &a, Fixed &b);


		Fixed					&operator=(Fixed const &obj);
		Fixed					&operator++();
		Fixed					&operator--();
		Fixed					operator--(int);
		Fixed					operator++(int);

		bool					operator>(Fixed const &obj)		const;
		bool					operator<(Fixed const &obj)		const;
		bool					operator>=(Fixed const &obj)	const;
		bool					operator<=(Fixed const &obj)	const;
		bool					operator==(Fixed const &obj)	const;
		bool					operator!=(Fixed const &obj)	const;

		Fixed					operator+(Fixed const &obj)		const;
		Fixed					operator-(Fixed const &obj)		const;
		Fixed					operator*(Fixed const &obj)		const;
		Fixed					operator/(Fixed const &obj)		const;

		float					toFloat()						const;
		int						toInt()							const;

	private:
		int						_value;
		int const				_bits;

};

std::ostream			&operator<<(std::ostream &o, Fixed const &src);

#endif
