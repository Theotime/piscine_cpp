/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.class.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/07 20:09:15 by triviere          #+#    #+#             */
/*   Updated: 2015/01/08 02:44:38 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.class.hpp"

/** MAIN CORRECTION

int main( void ) {
	Fixed a;
	Fixed const b( 10 );
	Fixed const c( 42.42f );
	Fixed const d( b );
	a = Fixed( 1234.4321f );
	std::cout << "a is " << a << std::endl;
	std::cout << "b is " << b << std::endl;
	std::cout << "c is " << c << std::endl;
	std::cout << "d is " << d << std::endl;
	std::cout << "a is " << a.toInt() << " as integer" << std::endl;
	std::cout << "b is " << b.toInt() << " as integer" << std::endl;
	std::cout << "c is " << c.toInt() << " as integer" << std::endl;
	std::cout << "d is " << d.toInt() << " as integer" << std::endl;
	return 0;
}

// END MAIN CORRECTION **/

Fixed::Fixed():
	_bits(FRACTIONAL_BITS)
{
	this->setRawBits(0);
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(Fixed const &obj):
	_bits(FRACTIONAL_BITS)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = obj;
}

Fixed::Fixed(int value):
	_bits(FRACTIONAL_BITS)
{
	this->setRawBits(roundf(value * std::pow(2, this->_bits)));
}

Fixed::Fixed(float value):
	_bits(FRACTIONAL_BITS)
{
	this->setRawBits(roundf(value * std::pow(2, this->_bits)));
}

Fixed::~Fixed() {
	std::cout << "Destructor called" << std::endl;
}

Fixed			&Fixed::operator=(Fixed const &obj) {
	std::cout << "Assignation operator called" << std::endl;
	this->_value = obj.getRawBits();
	return *this;
}

int				Fixed::getRawBits()		const {
	return (this->_value);
}

void			Fixed::setRawBits(int const raw) {
	this->_value = raw;
}

int				Fixed::toInt()				const {
	return this->getRawBits() / std::pow(2, this->_bits);
}

float			Fixed::toFloat()			const {
	return (float)this->getRawBits() / std::pow(2, this->_bits);
}

std::ostream		&operator<<(std::ostream &o, Fixed const &src) {
	o << src.toFloat();
	return (o);
}
